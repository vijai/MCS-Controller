package com.orpheusdroid.mcscontroller;

/**
 * Todo: Add class description here
 *
 * @author Vijai Chandra Prasad .R
 */
public class Const {
    public static String TAG = "MCS Controller";

    public static final int MCS_INFO_REQUEST_CODE = 100;
    public static final int MCS_SWITCH_STATUS_REQUEST_CODE = 200;
    public static final int MCS_SAVE_CONFIG_REQUEST_CODE = 300;
}
