package com.orpheusdroid.mcscontroller;

import android.preference.ListPreference;
import android.util.Log;
import android.view.KeyEvent;

import com.orpheusdroid.mcscontroller.interfaces.OnResultListener;
import com.orpheusdroid.mcscontroller.model.Result;
import com.topjohnwu.superuser.Shell;

/**
 * Todo: Add class description here
 *
 * @author Vijai Chandra Prasad .R
 */
public class Runtime {
    private static Runtime runtime;
    private Result cmdResult = new Result();
    private Shell.Result result;
    private OnResultListener listener;

    private Runtime(){

    }

    public static Runtime getRuntime() {
        if (runtime == null)
            runtime = new Runtime();
        return runtime;
    }

    public void setListener(OnResultListener listener) {
        this.listener = listener;
    }

    public Result runCommand(int requestCode, final Result cmdResult, String... command){
        Shell.su(command).submit(new Shell.ResultCallback() {
            @Override
            public void onResult(Shell.Result result) {
                cmdResult.setResultCode(result.getCode());
                cmdResult.setSuccessful(result.isSuccess());
                cmdResult.setStdErr(result.getErr());
                cmdResult.setStdOut(result.getOut());
            }
        });
        Log.d("Controller", cmdResult.toString());
        return cmdResult;
    }

    public void runCommand(final int requestCode, String... command){

        //Result cmdResult = new Result();
        Shell.su(command).submit(new Shell.ResultCallback() {
            @Override
            public void onResult(Shell.Result result) {
                cmdResult.setResultCode(result.getCode());
                cmdResult.setSuccessful(result.isSuccess());
                cmdResult.setStdErr(result.getErr());
                cmdResult.setStdOut(result.getOut());
                listener.onResultReceived(requestCode, cmdResult);
            }
        });
    }

    private void setResult(Result cmdResult, Shell.Result result) {
    }
}
