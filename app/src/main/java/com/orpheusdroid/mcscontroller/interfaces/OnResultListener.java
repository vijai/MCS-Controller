package com.orpheusdroid.mcscontroller.interfaces;

import com.orpheusdroid.mcscontroller.model.Result;

/**
 * Todo: Add class description here
 *
 * @author Vijai Chandra Prasad .R
 */
public interface OnResultListener {
    void onResultReceived(int RequestCode, Result result);
}
