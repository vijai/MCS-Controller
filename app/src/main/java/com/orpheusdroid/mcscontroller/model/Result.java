package com.orpheusdroid.mcscontroller.model;

import java.util.List;

/**
 * Todo: Add class description here
 *
 * @author Vijai Chandra Prasad .R
 */
public class Result {
    private int resultCode;
    private boolean isSuccessful;
    private List<String> stdOut;
    private List<String> stdErr;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<String> getStdOut() {
        return stdOut;
    }

    public void setStdOut(List<String> stdOut) {
        this.stdOut = stdOut;
    }

    public List<String> getStdErr() {
        return stdErr;
    }

    public void setStdErr(List<String> stdErr) {
        this.stdErr = stdErr;
    }

    @Override
    public String toString() {
        return "result:[ isSuccessful: " + isSuccessful() + ",\nresultCode: " + getResultCode() + ",\nstdOut: " + getStdOut() + ",\nstd Err: " + getStdErr() + "\n]";
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }
}
