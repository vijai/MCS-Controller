package com.orpheusdroid.mcscontroller.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.orpheusdroid.mcscontroller.BuildConfig;
import com.orpheusdroid.mcscontroller.R;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Element versionNameElement = new Element();
        versionNameElement.setTitle(getString(R.string.about_version_name,  BuildConfig.VERSION_NAME));

        Element versionBuildElement = new Element();
        versionBuildElement.setTitle(getString(R.string.about_build_type, BuildConfig.BUILD_TYPE));

        Intent mcsGitlabURL = new Intent();
        mcsGitlabURL.setAction(Intent.ACTION_VIEW);
        mcsGitlabURL.addCategory(Intent.CATEGORY_BROWSABLE);
        mcsGitlabURL.setData(Uri.parse("https://gitlab.com/vijai/MCS-Controller"));

        Element gitLab = new Element()
                .setTitle(getString(R.string.about_section_mcsc_fork_title))
                .setIconDrawable(R.drawable.ic_gitlab_logo)
                .setIntent(mcsGitlabURL)
                .setValue("https://gitlab.com/vijai/MCS-Controller");

        View aboutPage = new AboutPage(this)
                .setDescription(getString(R.string.about_section_description))
                .isRTL(false)
                .addGroup(getString(R.string.about_section_app_info_grp_title))
                .addItem(versionNameElement)
                .addItem(versionBuildElement)
                .addGroup(getString(R.string.about_section_connect_grp_title))
                .addEmail("contact@orpheusdroid.com")
                .addWebsite("https://orpheusdroid.com")
                .addPlayStore(getPackageName())
                .addItem(gitLab)
                .addGroup(getString(R.string.about_section_mcs_grp_title))
                .addWebsite("https://forum.xda-developers.com/apps/magisk/module-magic-charging-switch-cs-v2017-9-t3668427"
                        , getString(R.string.about_section_mcs_xda_support))
                .addGitHub("Magisk-Modules-Repo/Magic-Charging-Switch", getString(R.string.about_section_mcs_github_fork_title))
                .create();

        setContentView(aboutPage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
