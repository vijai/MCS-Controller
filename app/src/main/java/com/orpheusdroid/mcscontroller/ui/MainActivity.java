package com.orpheusdroid.mcscontroller.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.orpheusdroid.mcscontroller.Const;
import com.orpheusdroid.mcscontroller.R;
import com.orpheusdroid.mcscontroller.Runtime;
import com.orpheusdroid.mcscontroller.interfaces.OnResultListener;
import com.orpheusdroid.mcscontroller.model.Result;

import java.util.List;

import static com.orpheusdroid.mcscontroller.Const.MCS_INFO_REQUEST_CODE;

public class MainActivity extends AppCompatActivity implements OnResultListener, SwipeRefreshLayout.OnRefreshListener {
    private String lowerLimit;
    private String upperLimit;
    private boolean isMCSRunning;

    private EditText lowerLimitET;
    private EditText upperLimitET;
    private Switch mcsSwitch;
    private Button saveBtn;
    private SwipeRefreshLayout refreshLayout;

    private Runtime runtime;

    private String keyCtrl = " | input keyevent " + KeyEvent.KEYCODE_CTRL_LEFT + " & input keyevent " + KeyEvent.KEYCODE_C;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lowerLimitET = findViewById(R.id.lowerLimit);
        upperLimitET = findViewById(R.id.higherLimit);
        mcsSwitch = findViewById(R.id.mcsSwitch);
        saveBtn = findViewById(R.id.saveBtn);
        refreshLayout = findViewById(R.id.swipeRefresh);

        refreshLayout.setOnRefreshListener(this);

        runtime = Runtime.getRuntime();
        runtime.setListener(this);

        refresh();

        mcsSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(Const.TAG, mcsSwitch.isChecked() + "");
                switchMCSStatus(mcsSwitch.isChecked());
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveConfig();
            }
        });
    }

    private void refresh() {
        disableCardView();
        runtime.runCommand(MCS_INFO_REQUEST_CODE, "mcs -i");
    }

    private void disableCardView() {
        refreshLayout.setRefreshing(true);
        mcsSwitch.setEnabled(false);
        lowerLimitET.setEnabled(false);
        upperLimitET.setEnabled(false);
        saveBtn.setEnabled(false);
    }

    private void enableCardView() {
        if (refreshLayout.isRefreshing())
            refreshLayout.setRefreshing(false);
        mcsSwitch.setEnabled(true);
    }

    private void saveConfig() {
        Log.d(Const.TAG, "Saving config");
        String upperLimit = upperLimitET.getText().toString();
        String lowerLimit = lowerLimitET.getText().toString();

        if (upperLimit.isEmpty()) {
            upperLimitET.setError(getString(R.string.mcs_upper_limit_edit_text_empty_err_msg));
            return;
        }

        if (!lowerLimit.isEmpty() && Integer.parseInt(upperLimit) < Integer.parseInt(lowerLimit)) {
            upperLimitET.setError(getString(R.string.mcs_upper_limit_edit_text_incorrect_err_msg));
            return;
        }

        String cmd = "mcs " +
                upperLimitET.getText().toString() +
                " " +
                lowerLimitET.getText().toString() +
                keyCtrl;
        disableCardView();
        runtime.runCommand(Const.MCS_SAVE_CONFIG_REQUEST_CODE, cmd);
    }

    private void setMcsSwitch() {
        lowerLimitET.setEnabled(isMCSRunning);
        upperLimitET.setEnabled(isMCSRunning);
        saveBtn.setEnabled(isMCSRunning);
    }

    private void switchMCSStatus(boolean isMCSRunning) {
        String cmd;
        if (isMCSRunning) {
            cmd = "mcs";
        } else
            cmd = "mcs -e";

        runtime.runCommand(Const.MCS_SWITCH_STATUS_REQUEST_CODE, "mcs -s", cmd + keyCtrl);

    }

    private void setVals() {
        lowerLimitET.setText(lowerLimit);
        upperLimitET.setText(upperLimit);
        mcsSwitch.setChecked(isMCSRunning);
        setMcsSwitch();
        enableCardView();
    }

    private void showErrorAlert() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.mcs_not_reachable_alert_title)
                .setMessage(R.string.mcs_not_reachable_alert_message)
                .setCancelable(false)
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.this.finish();
                    }
                })
                .create()
                .show();
    }

    private void getVals(List<String> results) {
        for (String val : results) {
            if (val.contains("lowerLimit")) {
                lowerLimit = val.split("=")[1];
                Log.d(Const.TAG, val.split("=")[1]);
            }
            if (val.contains("higherLimit")) {
                upperLimit = val.split("=")[1];
                Log.d(Const.TAG, val.split("=")[1]);
            }

            if (val.contains("pauseDaemon")) {
                isMCSRunning = val.split("=")[1].equals("false");
            }
        }
    }

    private void dump(List<String> results) {
        for (String val : results) {
            if (val.contains("lowerLimit") || val.contains("higherLimit"))
                Log.d(Const.TAG, val);
        }
    }


    @Override
    public void onRefresh() {
        refresh();
    }

    @Override
    public void onResultReceived(int RequestCode, Result result) {
        Log.d(Const.TAG, result.toString());
        switch (RequestCode) {
            case Const.MCS_INFO_REQUEST_CODE:
                if (!result.isSuccessful()) {
                    showErrorAlert();
                    return;
                }
                getVals(result.getStdOut());
                setVals();
                break;
            case Const.MCS_SWITCH_STATUS_REQUEST_CODE:
                if (result.isSuccessful()) {
                    Log.d(Const.TAG, result.getStdOut().get(1));
                    isMCSRunning = !result.getStdOut().get(1).contains("paused");
                    setMcsSwitch();
                } else {
                    Toast.makeText(MainActivity.this
                            , R.string.mcs_pause_resume_failed_toast_msg
                            , Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            case Const.MCS_SAVE_CONFIG_REQUEST_CODE:
                if (result.isSuccessful()) {
                    Toast.makeText(this, R.string.mcs_save_successful_toast_msg, Toast.LENGTH_SHORT).show();
                }
                enableCardView();
                setMcsSwitch();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
